# First, change to the HOME directory. This way we won't import the 'loose'
# version of FieldPy which resides in the .git repo
import os
home = os.path.expanduser("~")
os.chdir(home)

# Then import the FieldPy version that should have been installed in the
# Anaconda python and run the FieldPy tests
from fieldpy.test import test_all
test_all.runTests()
