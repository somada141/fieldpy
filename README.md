# FieldPy v0.1.7

[![wercker status](https://app.wercker.com/status/0da687b52ec4264030166985ff5012db/m "wercker status")](https://app.wercker.com/project/bykey/0da687b52ec4264030166985ff5012db)

## Changelog

- v0.1.7, November 22 2014 -- Added Python 3 support
- v0.1.6, November 21 2014 -- Added compression options for datafield HDF5 IO
- v0.1.5, September 04 2014 -- Fixed nose tests.
- v0.1.4, September 04 2014 -- Still trying to get the tests to work (test-data wasn't transferring correctly).
- v0.1.3, September 04 2014 -- Added test package data for advanced tests.
- v0.1.2, September 04 2014 -- Added analysis and interpolation packages.
- v0.1.1, September 04 2014 -- Attempting to fix porting errors.
- v0.1.0, September 04 2014 -- First partial port of mathTools including only the `primitives` package.

## Background

`fieldpy` is a collection of primitives and tools for the analysis and manipulation of 2D and 3D rectilinear fields, e.g., resulting from FDTD simulations, experimental measurements on grids, etc. `fieldpy` is built upon entirely mainstream Python packages like NumPy, SciPy, Pandas, etc (see 'Requirements').

`fieldpy` is a revamped version of [`mathTools`](https://bitbucket.org/somada141/mathtools) which is now deprecated and should no longer be used. Its only being kept for archiving purposes.

### Features

`fieldpy` comprises several sub-packages, a quick break-down of which can be see below:

- `fieldpy.primitives` sub-package:
	- `fieldpy.primitives.point` module: Classes for 2D and 3D cartesian coordinate points and vectors (`pointCart2D` and `pointCart3D` classes). Classes are overloaded for all basic arithmetic operators and can be quickly converted/initialized to/from `list`, `tuple`, and `numpy.ndarray` objects. In addition, static methods for the calculation of mid-point coordinates and distance between points are also provided.
	- `fieldpy.primitives.idx` module: Classes for 2D and 3D indices (`idx2D` and `idx3D` classes).  Classes are overloaded for some basic arithmetic operators and can be quickly converted/initialized to/from `list`, `tuple`, and `numpy.ndarray` objects.
	- `fieldpy.primitives.axis` module: Class for non-uniform cartesian coordinate axes (`axis` class). The `axis` can be shifted, subset, or extended. Methods to find the index of a particular or nearest coordinate within the `axis` or test whether a coordinate is within the `axis` bounds are also provided.
	- `fieldpy.primitives.grid` module: Classes for 2D and 3D rectilinear grids which comprise 2 or 3 `axis` classes (`grid2D` and `grid3D` classes). Similarly to the `axis` class, `grid` objects can be shifted, subset, or extended. Methods exist to find indices of coordinates, test whether a set of coordinates is within the `grid` bounds, and calculate the areas/volumes of the `grid` cells.
	- `fieldpy.primitives.datafield` module: Classes for 2D and 3D datafields (`datafield2D` and `datafield3D` classes). These datafields wrap a `numpy.ndarray` containing the actual data and a `grid` object defining the spatial arrangement of the data. The `datafield2D` and `datafield3D` classes support both grid-cell-based and grid-node-based data. Wrapping the functionality provided by the `grid` class, datafields can be shifted, subset while the data can be normalized, scaled etc. Lastly, IO methods to write/read datafields to/from HDF5, Matlab, and MHD files are provided.
- `fieldpy.interpolation` sub-package:
	- `fieldpy.interpolation.interpolate`: Functions for the interpolation of 2D or 3D datafields from one rectilinear grid unto another.
- `fieldpy.analysis` sub-package:
	- `fieldpy.analysis.conCoAnalysis`: Classes for the extraction and analysis of connected-component in `datafield` objects.
	- `fieldpy.analysis.locExAnalysis`: Classes for the extraction and analysis of local-extrema (maxima/minima) in `datafield` objects.
	- `fieldpy.analysis.gammaAnalysis`: Classes for the cell-by-cell comparison of datafields based on the Gamma Dose-Distribution method [1, 2, 3].

## Documentation

[PyScience](http://pyscience.wordpress.com/) posts and IPython Notebook demonstrations are being prepared. Check-back soon

## Requirements

`fieldpy` depends on several mainstream Python packages:

- numpy >= 1.8.0
- pandas >= 0.14.0
- scipy >= 0.14.0
- h5py >= 2.2.1
- nose >= 1.3.3

## Installation

This package is already hosted on PyPI [here](https://pypi.python.org/pypi/FieldPy) and can be easily installed through pip as such:

```
pip install fieldpy
```

or straight from the source-code using `setuptools` as such:

```
python setup.py install
```

However, installing the required packages on a vanilla Python distribution is rather cumbersome as they depend on multiple C/C++ routines which need to be compiled upon installation. Thus, I strongly suggest the following:

- Use the freely available [Anaconda Python](https://store.continuum.io/cshop/anaconda/) distribution, which comes with pre-built binaries of all these packages, among many others, and supports all major operating systems. I've written a blog post about the advantages it offers [here](http://pyscience.wordpress.com/2014/09/01/anaconda-the-creme-de-la-creme-of-python-distros-3/).
- Install the dependencies with `conda` (see the aforementioned post)
- Install `fieldpy` with `pip` while skipping the dependencies as such:

```
pip install fieldpy --no-deps
```

## Development

### Workflow

The `fieldpy` development workflow is based on [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow/). Development commits are typically pushed in the `develop` branch with
new `feature` branches being created for the major features. Releases are pushed into the `master` branch.

### Testing & CI

`fieldpy` comes with tests written with `unittest` and batch-executed with `nose`. The distribution also comes with several auxiliary datasets used for testing the different aspects of the package. After installation you can easily run those tests from within a python session with the following code:

```
#!python

from fieldpy.test import test_all
test_all.runTests()
```

Every commit of `fieldpy` is being built and tested on the [Wercker](http://wercker.com/) CI system. The application is public and can be accessed through either clicking on the wercker badge at the top of this page or through [this](https://app.wercker.com/#applications/5408455dbb237fab11004f34) link.

## References

[1] Low, Daniel A., et al. "A technique for the quantitative evaluation of dose distributions." Medical physics 25.5 (1998): 656-661.

[2] Low, Daniel A., and James F. Dempsey. "Evaluation of the gamma dose distribution comparison method." Medical Physics 30.9 (2003): 2455-2464.

[3] de Bruijne, Maarten, et al. "Quantitative validation of the 3D SAR profile of hyperthermia applicators using the gamma method." Physics in medicine and biology 52.11 (2007): 3075.
